import {Form, Button } from 'react-bootstrap';
import { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'; 
import { faEye  } from '@fortawesome/free-solid-svg-icons';
import Swal from 'sweetalert2';

export default function ChangePassword({onShow, setOnShow, values}){
	const {id} = values;
	const [passwordType, setPasswordType] = useState('password')
	const [passwordType2, setPasswordType2] = useState('password')
	const [changedPassword, setChangedPassword] = useState({
		newpassword:'', 
		oldpassword:''
	})
	const {newpassword, oldpassword} = changedPassword
	const handleChange = (e) => {
		const {value, name} = e.target
		setChangedPassword(prevValue => {
			return { ...prevValue, [name]:value }
		})
	}

	const showPasswordFunc = () => {
		if(passwordType === 'password'){
			setPasswordType('text')
		} 
		else{
			setPasswordType('password')
		} 
	}
	const showPasswordFunc2 = () => {
		if(passwordType2 === 'password'){
			setPasswordType2('text')
		} 
		else{
			setPasswordType2('password')
		} 
	}
	const SuccessAlertUpdate = () => {
		Swal.fire({
			title:'FarmersPH.',
			text: 'Successfully updated password',
			icon: 'success',
			confirmButtonText: 'Close'
		})	
	}

	const ErrorAlert = () => { 
		Swal.fire({
			title:'FarmersPH.',
			text: 'Failed to change password.',
			icon: 'error',
			confirmButtonText: 'Try again'
		})
	}
	const onSubmitChangePassword = (e) => {
		console.log(changedPassword)
		e.preventDefault()
		let token = localStorage.getItem('token');
		fetch(`https://safe-bastion-71965.herokuapp.com/user/change-password/${id}`,{
			method: 'PATCH',
			headers: { 
				"Content-Type": "application/json",
				Authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
				newPassword: newpassword,
				oldPassword: oldpassword
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				SuccessAlertUpdate()
				setOnShow(false);
			}else ErrorAlert()
		})
		setChangedPassword(() => {
			return{ newpassword: '', oldpassword:'' }
		})
	}
	const onCancelSubmitChangePassword = () => {
		setOnShow(false);
		setChangedPassword(() => {
			return{ newpassword: '', oldpassword:'' }
		})
	}

	return <div>
		{onShow? <Form>
			<Form.Group className="mb-3">
			<Form.Label className="form-label-header">Old Password</Form.Label>
			<div className="password-container">
			<Form.Control 
				onChange={handleChange}
				size="sm" 
				type={passwordType}
				name="oldpassword"
				values={oldpassword}
				placeholder="Please enter your last name" />
			<Button onClick={showPasswordFunc} value="passwordBtn" size="sm" className="form-button__seePassword"><FontAwesomeIcon icon={faEye}/></Button>
			</div>
			</Form.Group>
			<Form.Group className="mb-3">
			<Form.Label className="form-label-header">New Password</Form.Label>
			<div className="password-container">
			<Form.Control 
				onChange={handleChange}
				size="sm" 
				type={passwordType2}
				name="newpassword" 
				values={newpassword} 
				placeholder="Please enter your last name" />
				<Button onClick={showPasswordFunc2} value="passwordBtn" size="sm" className="form-button__seePassword" ><FontAwesomeIcon icon={faEye}/></Button>
			</div>
			</Form.Group>
			<Button variant="danger" onClick={onCancelSubmitChangePassword} className="changePasswordBtn">Cancel</Button>
			<Button variant="warning" onClick={onSubmitChangePassword} className="changePasswordBtn">Save Changes</Button>
		</Form>: null}
	</div>
}