import '../styles/Rating.css';
import Comment from '../components/Comment';
import { Fragment, useState } from 'react';

export default function Rating(){
	let [totalRate, isTotalRate] = useState(0)

	let comment = [
		{
			name:"James Lawrence Villanueva",
			date: new Date(),
			description:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."
		},
	]

	let CommentComponent = comment.map((comment, index) => {
		return <Comment 
					key={index} 
					commentName={comment.name} 
					commentDate={comment.date}
					commentDescription={comment.description} 
				/>
	})

	return <Fragment>
		<div className="rating-container">
			<p className="rating-header">Rating and Review</p>
			<div className="rating-box">
				<p className="rating-rate">{totalRate}</p>
				<p className="rating-maxRate">/5.0</p>
			</div>
			<p>Stars<small className="rating-totalNumberOfBuyer">(260)</small></p>
		</div>
		{CommentComponent}
	</Fragment>
}