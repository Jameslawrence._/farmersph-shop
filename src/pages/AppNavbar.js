import '../styles/AppNavbar.css';
import { Fragment, default as React, useContext } from 'react';
import { Navbar,Nav, Form, Button, FormControl, NavDropdown } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../userContext';
// Fontawesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'; 
import { faSearch  } from '@fortawesome/free-solid-svg-icons'; 
/*import { faFacebookF, faInstagram, faTwitter, faLinkedinIn } from "@fortawesome/free-brands-svg-icons"*/

export default function AppNavbar(){

	const {user} = useContext(UserContext);
	const {firstname, lastname} = user;

	console.log(user)
	return <Fragment> 
		<Navbar fixed="top" expand="md" className="navbar">
		  <Navbar.Brand as={Link} to="/" exact="true" className="navbar-brand">Farmers<span className="navbar-brand__country">PH</span>.</Navbar.Brand>
		  <Navbar.Toggle aria-controls="basic-navbar-nav" />
		  <Navbar.Collapse id="basic-navbar-nav">
		    <Nav className="mr-auto">

		    </Nav>
		    <Form inline="true" className="form">
		      <FormControl type="text" placeholder="Search" size="sm" className="form-control"/>
		      <Button size="sm" className="form-button__search"><FontAwesomeIcon icon={faSearch}/></Button>
		    </Form>
		    <Nav className="ml-auto">
		    {(user.id !== null)? <Fragment>
			        <NavDropdown title={<span  className="navbar-link">{firstname +' '+ lastname}</span>} id="basic-nav-dropdown">
			          <NavDropdown.Item as={Link} to="/profile" exact="true"  className="navbar-link">Manage Profile</NavDropdown.Item>
			          <NavDropdown.Item href="#action/3.1"  className="navbar-link">My Cart</NavDropdown.Item>
			          <NavDropdown.Item href="#action/3.3" className="navbar-link">My Wishlist</NavDropdown.Item>
			        </NavDropdown>		    	
		      		<Nav.Link as={Link} to="/logout" exact="true" className="navbar-link">Logout</Nav.Link>
			   </Fragment>	
		      :<Fragment>
		      		<Nav.Link as={Link} to="/signup" exact="true" className="navbar-link">Sign up</Nav.Link>
		      		<Nav.Link as={Link} to="/login" exact="true" className="navbar-link">Login</Nav.Link>
		       </Fragment>
		    }
		    </Nav>
		  </Navbar.Collapse>
		</Navbar>
	</Fragment>
}

