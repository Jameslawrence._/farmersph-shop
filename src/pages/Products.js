import '../styles/Products.css';

import { Fragment, useState, useEffect } from 'react';
import Cards from '../components/Cards';
import { Row, Col } from 'react-bootstrap';

export default function Product(){
	const [products, setProducts] = useState([])

	useEffect(() => {
		fetch('https://safe-bastion-71965.herokuapp.com/product',{
			method: 'GET',
			headers: { 'Content-Type':'application/json'}, 
		})
		.then(res => res.json())
		.then(data => {
			setProducts(data);
		})	
	},[])



	const Products = products.map(product => {
		const productArrName = product.name;
		const productName = productArrName.charAt(0).toUpperCase() + productArrName.slice(1); 

		return <Col md={4} key={product._id}> 
			<div className="product-box">
			<Cards id={product._id} title={productName} price={product.price}/> 
			</div>
		</Col>
	})

	return <Fragment>
		<Row className="products-container">
			{(products)? Products: <h1>Market is empty</h1> }
		</Row>
	</Fragment>
	
}				