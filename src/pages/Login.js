import '../styles/Login.css';
import { Fragment, useState, useContext } from 'react'; 
import { Form, Button } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'; 
import { faEye  } from '@fortawesome/free-solid-svg-icons'; 
import { Link, useNavigate, Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../userContext';

export default function Login(){
	const { user, setUser } = useContext(UserContext);
	const [passwordVisible, setIsPasswordVisible] = useState(true);
	const [passwordType, setPasswordType] = useState('password');
	const [ userLoginInfo, setUserLoginInfo ] = useState({
		email:'',
		password:''
	})

	const history = useNavigate();
	const {email, password} = userLoginInfo
 	const makePasswordVisible = () => {
		if(passwordVisible){
			setPasswordType('text')
			setIsPasswordVisible(false)
		}
		else{
			setPasswordType('password')	
			setIsPasswordVisible(true)
		} 
	}
	
	const onChangeHandler = (e) => {
		const { name, value } = e.target;
		return setUserLoginInfo(prevValue => {
			return { ...prevValue, [name]:value }
		})
	}
	const SuccessAlert = () => {
		Swal.fire({
			title:'FarmersPH.',
			text: 'Successfully Login!',
			icon: 'success',
			confirmButtonText: 'Proceed'
		})	
	}

	const ErrorAlert = () => { 
		Swal.fire({
			title:'FarmersPH.',
			text: 'Email or Password Incorrect. Failed to login!',
			icon: 'error',
			confirmButtonText: 'Back'
		})
	}
	const login = (e) => {
		e.preventDefault(); 
		fetch('https://safe-bastion-71965.herokuapp.com/user/login', {
			method:'POST',
			headers:{'Content-Type':'application/json'},
			body: JSON.stringify({
				email:email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(typeof data.access !== "undefined"){
				localStorage.setItem('token', data.access)
				retrieveUserDetails(data.access);
				SuccessAlert();
				history('/products');
			}else{
				ErrorAlert();
				history('/login');
			}
		})	
		setUserLoginInfo(() => { return {email:'', password:''}}) 
	}

	const retrieveUserDetails = (token) => {
		fetch('https://safe-bastion-71965.herokuapp.com/user/profile',{
			headers:{ Authorization: `Bearer ${token}`}
		})
		.then(res => res.json())
		.then(data => {
			setUser({
				id: data._id,
				isAdmin: data.isAdmin,
				firstname: data.fullName[0].givenName,
				lastname: data.fullName[0].familyName
			})
			fetch(`https://safe-bastion-71965.herokuapp.com/user/disable/${data._id}`,{
				method: 'PATCH',
				headers:{
					'Content-Type':'application/json',
					Authorization: `Bearer ${token}`
				},
				body: JSON.stringify({
					isActive: true 
				})
			})
		})
	}

			
	return <Fragment>
		{user.id? <Navigate to='/profile'/>:<div className="login-container">
			<div className="form-box">
			<p className="login-header">Welcome back.</p>
			<p className="login-subHeader">Login to your account.</p>
			<Form>
				<Form.Group className="mb-3">
				    <Form.Label className="form-label-login">Email</Form.Label>
				    <Form.Control 
				    	onChange={onChangeHandler} 
				    	size="sm" 
				    	type="email" 
				    	name="email"
				    	value={email}
				    	placeholder="Please enter your email" />
				  </Form.Group>
				<Form.Group className="mb-3" >
				    <Form.Label className="form-label-login">Password</Form.Label>
				    <div className="password-container">
				    <Form.Control 
				    	onChange={onChangeHandler} 
				    	size="sm" 
				    	type={passwordType} 
				    	name="password"
				    	value={password}
				    	placeholder="Please enter password" />
					  <Button onClick={makePasswordVisible} value="passwordBtn" size="sm" className="form-button__seePassword" ><FontAwesomeIcon icon={faEye}/></Button>
					</div>
				  </Form.Group>
				<Button className="btn-login" onClick={ login }>Login</Button>
			</Form>
			<div className="form-option-box">
				<Link to="/signup" exact="true">create account</Link>
				<Link to="/forgot-password" exact="true">forgot password?</Link>
			</div>
			</div>
		</div>}
	</Fragment>
}