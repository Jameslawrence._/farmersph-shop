import '../styles/ProductDetail.css';
import { Fragment, useState, useEffect, useContext } from 'react';
import { Row, Col, Button, Image } from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom';
import Rating from './Rating';
import UserCOntext from '../userContext';

export default function ProductDetail(props){

	const {user} = useContext(UserCOntext);
	const {productId} = useParams();
	const [product, setProduct] = useState({})
	let {description, name, stocked, _id} = product 

	const availableStocks = stocked;
	let [stockedAvailable, setStockedAvailable] = useState(availableStocks) 
	useEffect(() => {
		fetch(`https://safe-bastion-71965.herokuapp.com/product/${productId}`,{
			method: 'GET',
			headers: {'Context-Type':'application/json'},
		})
		.then(res => res.json())
		.then(data => {
			setProduct(data)
		})
	},[])

	console.log(stocked)
	console.log(stockedAvailable)

	let [totalBought, setTotalBought] = useState(0)

	function addProduct(){
		setStockedAvailable(stocked)
		setTotalBought(totalBought += 1)
		setStockedAvailable(stockedAvailable -= 1)		
	}

	function lessenProduct(){
		if(totalBought > 0){
			setTotalBought(totalBought -= 1)		
			setStockedAvailable(stockedAvailable += 1)		
		} 
	}

	const addToCart = () => {
		const userId = user.id
		const token = localStorage.getItem('token')

		fetch('https://safe-bastion-71965.herokuapp.com/user/purchase', {
			method: 'POST',
			headers:{
				'Content-Type':'application/json',
				Authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
				productId: _id,
				numberProductsBought: totalBought
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
		})
	}
	return <Fragment>
		<div className="product-detail-container">
			<Row>
				<Col>
					<div className="product-detail__BoxImg">
						<Image src="https://images.unsplash.com/photo-1444731961956-751ed90465a5?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=387&q=80" className="product-detail__img"/>
					</div>
				</Col>
				<Col>
					<div className="product-detail__content">
						<p className="product-detail__name">{name}</p>
						<div className="product-detail__rating">
							<p>Stars </p>
							<small>(209)</small>
						</div>
						<p className="product-detail__description">{description}</p>
						<p>Available Stocks: {stockedAvailable}</p>
						<div className="product-detail__quantity">
							<p className="product-detail__quantity_title">Quantity</p>
							<Button size="sm" variant="light" className="product-detail__less" onClick={lessenProduct} disabled={totalBought > 0? false:true}>-</Button>
							<p className="product-detail__howMany">{totalBought}</p>
							<Button size="sm" variant="light" className="product-detail__more" onClick={addProduct}>+</Button>
						</div>
						<div className="product-detail__process">
							{user.id === null? <Button size="sm"  as={Link} to='/login' exact="true" className="product-detail__login">Login</Button>:	
								<Fragment>
									<Button size="sm" variant="light" className="product-detail__buyNowBtn">Buy Now</Button>	
									<Button size="sm" variant="light" onClick={addToCart} className="product-detail__addToCartBtn">Add to Cart</Button>	
								</Fragment>
							}
						</div>
					</div>
				</Col>
			</Row>
			<Rating/>
		</div>
	</Fragment>
}